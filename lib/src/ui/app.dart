import 'package:ble/device_view.dart';
import 'package:ble/src/ui/modal/ble_discovery.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:provider/provider.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BLE Test',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: "Test "),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String? title;

  const MyHomePage({Key? key, this.title}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final int _navigationIndex = 0;

  @override
  Widget build(BuildContext context) =>
      Consumer<BleStatus?>(builder: (_, status, __) {
        Icon? _icon;
        if (status == null || status != BleStatus.ready) {
          _icon = const Icon(Icons.bluetooth_disabled);
        } else {
          _icon = const Icon(Icons.bluetooth);
        }
        return Scaffold(
          appBar: AppBar(
              title: Text(widget.title ?? "empty"),
              actions: [IconButton(onPressed: () {}, icon: _icon)]),
          floatingActionButton: FloatingActionButton(
            onPressed: _showBleModal,
            child: const Icon(Icons.bluetooth),
          ),
          bottomNavigationBar: BottomNavigationBar(
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: "Accueil"),
              BottomNavigationBarItem(
                icon: Icon(Icons.album),
                label: 'Lecteur',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.star),
                label: 'Favoris',
              ),
            ],
            currentIndex: _navigationIndex,
          ),
          body: const DeviceView(allowEdit: true, id: "aaaa-bbbb-cccc"),
        );
      });

  void _showBleModal() {
    showGeneralDialog(
      context: context,
      pageBuilder: (_, __, ___) {
        return const BleDiscoveryModal();
      },
      barrierDismissible: true,
      barrierLabel: "connard"
    );
  }

  void postSnackbar(String text) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(text)));
  }
}
