import 'package:ble/src/ui/app.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionView extends StatelessWidget {
  const PermissionView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    checkPermission(context);
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text(
                "La permission localisation est nécessaire pour pouvoir utiliser "
                "le Bluetooth Low Energy."),
            TextButton.icon(
              style: TextButton.styleFrom(
                textStyle: const TextStyle(color: Colors.blue),
                backgroundColor: Colors.black12,
              ),
              icon: const Icon(Icons.bluetooth),
              label: const Text('Activer permission'),
              onPressed: ((){checkPermission(context);}),
            )
          ],
        ),
      ),
    );
  }

  Future<void> checkPermission(BuildContext context) async {
    var statusLocation = await Permission.locationWhenInUse.request();
    var statusBluetooth = await Permission.bluetooth.request();
    if (statusLocation.isGranted && statusBluetooth.isGranted) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => const App()),
          ModalRoute.withName("/app"));
    }
  }
}
