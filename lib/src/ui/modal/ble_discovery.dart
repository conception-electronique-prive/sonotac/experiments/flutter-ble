import 'package:ble/src/ble/ble_scanner.dart';
import 'package:flutter/material.dart';
import 'package:flutter_reactive_ble/flutter_reactive_ble.dart';
import 'package:provider/provider.dart';

class BleDiscoveryModal extends StatelessWidget {
  const BleDiscoveryModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Consumer2<BleScanner, BleScannerState?>(
        builder: (_, bleScanner, bleScannerState, __) => _DeviceList(
          scannerState: bleScannerState ??
              const BleScannerState(
                discoveredDevices: [],
                scanIsInProgress: false,
              ),
          startScan: bleScanner.startScan,
          stopScan: bleScanner.stopScan,
        ),
      );
}

class _DeviceList extends StatefulWidget {
  const _DeviceList(
      {required this.scannerState,
      required this.startScan,
      required this.stopScan});

  final BleScannerState scannerState;
  final void Function(List<Uuid>) startScan;
  final VoidCallback stopScan;

  @override
  _DeviceListState createState() => _DeviceListState();
}

class _DeviceListState extends State<_DeviceList> {
  @override
  void dispose() {
    widget.stopScan();
    super.dispose();
  }

  void _startScanning() {
    if (!widget.scannerState.scanIsInProgress) {
      print("Starting scan?");
      widget.startScan([Uuid.parse("4fafc201-1fb5-459e-8fcc-c5c9c331914b")]);
    }
  }

  @override
  Widget build(BuildContext context) {
    _startScanning();
    return Dialog(
      child: FittedBox(
        fit: BoxFit.fitWidth,
        child: Column(
          children: [
            Row(
              children: [
                const Text(
                    "Appuyer sur le bouton pour activer le mode d'appairage du SonoTac"),
                IconButton(
                  icon: const Icon(Icons.bluetooth),
                  onPressed: _startScanning,
                ),
              ],
            ),
            Row(
              children: [
                widget.scannerState.scanIsInProgress
                    ? const Text("Scan en cours")
                    : const Text("Scan terminé"),
                const CircularProgressIndicator(),
              ],
            ),
            SizedBox(
              height: 300,
              width: 300,
              child: ListView(
                padding: const EdgeInsets.all(8),
                children: widget.scannerState.discoveredDevices
                    .map(
                      (device) => ListTile(
                        title: Text(device.name),
                        subtitle: const Text(""),
                        leading: const Icon(Icons.bluetooth),
                        onTap: () async {
                          widget.stopScan();
                          print("tap on ${device.name}");
                          Navigator.of(context, rootNavigator: true).pop();
                        },
                      ),
                    )
                    .toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
