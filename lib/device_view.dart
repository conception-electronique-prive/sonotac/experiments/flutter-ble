import 'package:flutter/material.dart';

class DeviceView extends StatelessWidget {
  final String? name;
  final String? id;
  final bool? allowEdit;
  final String? bleState;
  final String? license;

  const DeviceView(
      {Key? key,
      this.name,
      this.id,
      this.allowEdit,
      this.bleState,
      this.license})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Expanded(
            child: Align(
              child: Column(
                children: [
                  Align(
                    child: Text(name ?? "SonoTac"),
                    alignment: Alignment.topLeft,
                  ),
                  Table(
                    children: [
                      TableRow(children: [
                        const Text("Bluetooth"),
                        Text(bleState ?? "Not Connected")
                      ]),
                      TableRow(children: [
                        const Text("License"),
                        Text(license ?? "Unknown")
                      ]),
                      TableRow(
                          children: [const Text("ID"), Text(id ?? "Unknown")])
                    ],
                  ),
                ],
              ),
              alignment: Alignment.topLeft,
            ),
          ),
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
              onPressed: () {},
              icon: const Icon(Icons.edit),
            ),
          )
        ],
      ),
    );
  }
}
